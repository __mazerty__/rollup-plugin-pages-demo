import {ArrowPathIcon} from "@heroicons/react/24/outline"

export default () => (
    <section className="min-h-80 flex">
        <ArrowPathIcon className="m-auto w-24 h-24 text-gray-400 animate-spin"/>
    </section>
)
