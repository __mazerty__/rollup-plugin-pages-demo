import ReactDOM from "react-dom/client"
import React from "react"
import {createBrowserRouter, redirect, RouterProvider} from "react-router-dom"
import Layout from "./Layout"
import pages from "virtual:pages"
import "./index.css"

const Blog = React.lazy(() => import("./Blog"))
const Wrap = React.lazy(() => import("./Wrap"))

ReactDOM.createRoot(document.getElementById("root")).render(
    <React.StrictMode>
        <RouterProvider router={createBrowserRouter([{
            element: <Layout/>,
            children: [{
                index: true,
                element: <Blog/>
            }, {
                element: <Wrap/>,
                children: pages.map(page => {
                    const Page = React.lazy(page.import)
                    return {
                        path: page.key,
                        element: <Page/>,
                        handle: page
                    }
                })
            }, {
                path: "*",
                loader: () => redirect("/")
            }]
        }])}/>
    </React.StrictMode>
)
