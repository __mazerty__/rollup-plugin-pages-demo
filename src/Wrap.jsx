import {Outlet, useMatches} from "react-router-dom"

export default () => {
    const current_page = useMatches().find(i => i.handle)?.handle

    return <>
        <div className="text-gray-400">{current_page.date}</div>
        <div className="text-4xl mt-2">{current_page.title}</div>
        <div className="prose max-w-none mt-10">
            <Outlet/>
        </div>
    </>
}
