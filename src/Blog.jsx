import {Link} from "react-router-dom"
import pages from "virtual:pages"

export default () => <>
    <div className="mt-5 mb-12 text-gray-900">
        <p> This is a demo website for <a className="underline" href="https://www.npmjs.com/package/@__mazerty__/rollup-plugin-pages">rollup-plugin-pages</a>, a Rollup/Vite plugin that can be used to extract metadata from a directory of markdown files with frontmatter (such as blog pages). </p>
        <p> Even though this website uses React, the plugin in itself is a pure JS module and produces an array of objects that you can use as you see fit. </p>
        <p> You'll still need a library such as <a className="underline" href="https://mdxjs.com">MDX</a> or <a className="underline" href="https://github.com/unplugin/unplugin-vue-markdown">unplugin-vue-markdown</a> to turn your markdown files into components, but this is outside of the scope of this plugin :) </p>
        <p> By the way, clicking on a blog post below will show you Tailwind's <a className="underline" href="https://tailwindcss.com/docs/typography-plugin">Typography plugin</a> in action ;) </p>
    </div>
    {pages.map(page => (
        <Link to={page.key} key={page.key}>
            <div className="mb-4">
                <div className="flex flex-col-reverse sm:flex-row">
                    <div className="text-xl mr-auto">{page.title}</div>
                    <div className="text-sm text-gray-400">{page.date}</div>
                </div>
                <div className="text-gray-400">{page.description}</div>
            </div>
        </Link>
    ))}
</>
