import {Suspense, useEffect} from "react"
import {Link, Outlet, ScrollRestoration, useMatches} from "react-router-dom"
import Loading from "./Loading"
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome"
import {faCreativeCommons, faCreativeCommonsBy, faCreativeCommonsNcEu, faCreativeCommonsSa} from "@fortawesome/free-brands-svg-icons"
import Logo from "./logo.svg?react"

export default () => {
    const current_page = useMatches().find(i => i.handle)?.handle

    useEffect(() => {
        document.title = (current_page ? current_page.title + " - " : "") + "Blog"
    })

    return <>
        <ScrollRestoration/>
        <nav className="fixed w-full px-5 z-10 bg-gray-50 border-b">
            <div className="max-w-4xl mx-auto flex items-center h-16 text-xl">
                <Link to="/" className="flex">
                    <Logo className="h-7 w-7 mr-1.5"/>
                    <div>Blog</div>
                </Link>
                {current_page && <Link to={current_page.key} className="ml-2 text-gray-400">&gt; {current_page.title}</Link>}
            </div>
        </nav>
        <main className="flex flex-col min-h-screen px-5 pt-20">
            <div className="w-full max-w-4xl mx-auto">
                <Suspense fallback={<Loading/>}><Outlet/></Suspense>
            </div>
            <footer className="mt-auto pt-20 text-gray-400 text-xs text-center">
                <div><FontAwesomeIcon icon={faCreativeCommons}/> <FontAwesomeIcon icon={faCreativeCommonsBy}/> <FontAwesomeIcon icon={faCreativeCommonsNcEu}/> <FontAwesomeIcon icon={faCreativeCommonsSa}/></div>
                <div>Licensed under the <a href="https://creativecommons.org/licenses/by-nc-sa/4.0">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.</div>
                <div>Powered by <a href="https://reactjs.org">React</a>, <a href="https://vitejs.dev">Vite</a>, <a href="https://tailwindcss.com">Tailwind</a>, <a href="https://fontsource.org">Fontsource</a>, <a href="https://fontawesome.com">Font Awesome</a> and <a href="https://gitlab.com/__mazerty__/rollup-plugin-pages-demo">GitLab</a>.</div>
            </footer>
        </main>
    </>
}
