import {defineConfig} from "vite"
import react from "@vitejs/plugin-react"
import svgr from "vite-plugin-svgr"
import mdx from "@mdx-js/rollup"
import remarkFrontmatter from "remark-frontmatter"
import pages from "@__mazerty__/rollup-plugin-pages"
import tailwind from "tailwindcss"
import typography from "@tailwindcss/typography"
import defaultTheme from "tailwindcss/defaultTheme.js"

export default defineConfig({
    plugins: [
        react(),
        svgr(),
        mdx({remarkPlugins: [remarkFrontmatter]}),
        pages(__dirname + "/src/pages")
    ],
    css: {
        postcss: {
            plugins: [
                tailwind({
                    content: ["./index.html", "./src/**/*.jsx"],
                    theme: {
                        extend: {
                            fontFamily: {
                                sans: ["Ubuntu", ...defaultTheme.fontFamily.sans]
                            },
                            minHeight: {
                                80: "80vh"
                            }
                        }
                    },
                    plugins: [typography()]
                })
            ]
        }
    }
})
