# rollup-plugin-pages-demo

This is a demo website for [rollup-plugin-pages](https://www.npmjs.com/package/@__mazerty__/rollup-plugin-pages), a Rollup/Vite plugin that can be used to extract metadata from a directory of markdown files with frontmatter (such as blog pages).  
Even though this website uses React, the plugin in itself is a pure JS module and produces an array of objects that you can use as you see fit.  
You'll still need a library such as [MDX](https://mdxjs.com) or [unplugin-vue-markdown](https://github.com/unplugin/unplugin-vue-markdown) to turn your markdown files into components, but this is outside the scope of this plugin :)

For more information, go to [gitlab.com/\_\_mazerty\_\_/rollup-plugin-pages](https://gitlab.com/__mazerty__/rollup-plugin-pages).

## How to play with the demo

You can either:
* go to [rollup-plugin-pages.mazerty.fr](https://rollup-plugin-pages.mazerty.fr)
* experiment with the code from your browser with [Gitpod](https://gitpod.io/#https://gitlab.com/__mazerty__/rollup-plugin-pages-demo)
* or run it locally

## How to run the demo locally

You need to have at least Node.js v22 [installed](https://nodejs.org/en/learn/getting-started/how-to-install-nodejs), then you can run:

```
git clone https://gitlab.com/__mazerty__/rollup-plugin-pages-demo
cd rollup-plugin-pages-demo
npm install
npm run dev
```
